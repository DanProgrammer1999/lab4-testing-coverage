package com.hw.db.controllers.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

@DisplayName("thread DAO")
public class threadDaoTests {
    @Test
    @DisplayName("TreeSort with true description")
    void TreeSortTest1() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        ThreadDAO threadDAO = new ThreadDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ThreadDAO.treeSort(1, 1, 1, false);
        var query = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > " +
                "(SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class)
        );
    }

    @Test
    @DisplayName("TreeSort with false description")
    void TreeSortTest2() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        ThreadDAO threadDAO = new ThreadDAO(mockJdbc);
        PostDAO.PostMapper POST_MAPPER = new PostDAO.PostMapper();
        ThreadDAO.ThreadMapper THREAD_MAPPER = new ThreadDAO.ThreadMapper();
        ThreadDAO.treeSort(1, 1, 1, true);
        var query = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < " +
                "(SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class));
    }
}
