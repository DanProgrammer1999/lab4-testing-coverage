package com.hw.db.controllers.DAO;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

@DisplayName("user DAO")
public class userDaoTests {
    private User user;
    private JdbcTemplate jdbc;

    @BeforeEach
    void resetUser() {
        user = new User("admin", null, null, null);

        jdbc = Mockito.mock(JdbcTemplate.class);
        UserDAO dao = new UserDAO(jdbc);
    }

    @Test
    @DisplayName("Do not change anything")
    void changeNothing() {
        UserDAO.Change(user);
        Mockito.verify(jdbc, Mockito.never()).update(Mockito.anyString(), Mockito.anyCollection());
    }

    @Test
    @DisplayName("Change user email")
    void testChangeEmail() {
        user.setEmail("admin@example.com");
        UserDAO.Change(user);
        var query = "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;";
        Mockito.verify(jdbc).update(query, user.getEmail(), user.getNickname());
    }

    @Test
    @DisplayName("Change user full name")
    void testChangeFullName() {
        user.setFullname("Admin Admin");
        UserDAO.Change(user);
        var query = "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;";
        Mockito.verify(jdbc).update(query, user.getFullname(), user.getNickname());
    }

    @Test
    @DisplayName("Change user info")
    void testChangeAbout() {
        user.setAbout("Nothing important");
        UserDAO.Change(user);
        var query = "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;";
        Mockito.verify(jdbc).update(query, user.getAbout(), user.getNickname());
    }
}
