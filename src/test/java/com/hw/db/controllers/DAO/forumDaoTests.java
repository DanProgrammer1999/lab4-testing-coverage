package com.hw.db.controllers.DAO;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

@DisplayName("forum DAO")
public class forumDaoTests {
    @Test
    @DisplayName("User gets list of threads 1")
    void ThreadListTest1() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, null, null);
        var query = "SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("User gets list of threads 2")
    void ThreadListTest2() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, "09.09.2009", false);
        var query = "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)" +
                "::TIMESTAMP WITH TIME ZONE ORDER BY created;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("User gets list of threads 3")
    void ThreadListTest3() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", null, "09.09.2009", true);
        var query = "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)" +
                "::TIMESTAMP WITH TIME ZONE ORDER BY created desc;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("User gets list of threads 4")
    void ThreadListTest4() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, "09.09.2009", true);
        var query = "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)" +
                "::TIMESTAMP WITH TIME ZONE ORDER BY created desc LIMIT ?;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("User gets list of threads 5")
    void ThreadListTest5() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, "09.09.2009", false);
        var query = "SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)" +
                "::TIMESTAMP WITH TIME ZONE ORDER BY created LIMIT ?;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    @DisplayName("User gets list of threads 6")
    void ThreadListTest6() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new ThreadDAO.ThreadMapper();
        ForumDAO.ThreadList("slug", 10, null, true);
        var query = "SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc LIMIT ?;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(ThreadDAO.ThreadMapper.class)
        );
    }

    @Test
    void testUserList1() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", null, null, null);
        var query = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserList2() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", null, null, true);
        var query = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname desc;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserList3() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", null, "09.09.2009", false);
        var query = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext AND  nickname > (?)::citext ORDER BY nickname;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserList4() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", null, "09.09.2009", true);
        var query = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext AND  nickname < (?)::citext ORDER BY nickname desc;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserList5() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", 10, null, null);
        var query = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname LIMIT ?;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserList6() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", 10, null, true);
        var query = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext ORDER BY nickname desc LIMIT ?;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserList7() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", 10, "09.09.2009", null);
        var query = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void testUserList8() {
        JdbcTemplate mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        new UserDAO.UserMapper();

        ForumDAO.UserList("slug", 10, "09.09.2009", true);
        var query = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)" +
                "::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;";
        Mockito.verify(mockJdbc).query(
                Mockito.eq(query),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
        );
    }
}
