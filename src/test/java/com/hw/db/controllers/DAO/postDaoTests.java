package com.hw.db.controllers.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.TimeUnit;

@DisplayName("post DAO")
public class postDaoTests {

    private final Post post1 = new Post(
            "User1",
            new Timestamp(System.currentTimeMillis()),
            "Forum1",
            "Message",
            0,
            0,
            false
    );
    private final Post post2 = new Post(
            "User2",
            new Timestamp(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(2)),
            "Forum2",
            "Another message",
            0,
            0,
            false
    );

    private JdbcTemplate jdbc;

    @BeforeEach
    void initializeDao() {
        post1.setId(0);
        post2.setId(1);

        jdbc = Mockito.mock(JdbcTemplate.class);
        PostDAO dao = new PostDAO(jdbc);

        for (Post post : List.of(post1, post2)) {
            Mockito.when(
                    jdbc.queryForObject(
                            Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                            Mockito.any(PostDAO.PostMapper.class),
                            Mockito.eq(post.getId())
                    )
            ).thenReturn(post);
        }
    }

    @Test
    @DisplayName("Change nothing")
    void changeNothing() {
        PostDAO.setPost(post1.getId(), post1);
        Mockito.verify(jdbc, Mockito.never()).update(Mockito.anyString(), Mockito.anyCollection());
    }

    @Test
    @DisplayName("Change post author")
    void changeAuthor() {
        post2.setMessage(post1.getMessage());
        post2.setCreated(post1.getCreated());

        PostDAO.setPost(post1.getId(), post2);
        Mockito.verify(jdbc).update(
                "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;",
                post2.getAuthor(), post1.getId()
        );
    }

    @Test
    @DisplayName("Change post message")
    void changeMessage() {
        post2.setAuthor(post1.getAuthor());
        post2.setCreated(post1.getCreated());

        PostDAO.setPost(post1.getId(), post2);
        Mockito.verify(jdbc).update(
                "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;",
                post2.getMessage(), post1.getId()
        );
    }

    @Test
    @DisplayName("Change post created")
    void changeCreated() {
        post2.setAuthor(post1.getAuthor());
        post2.setMessage(post1.getMessage());

        PostDAO.setPost(post1.getId(), post2);
        Mockito.verify(jdbc).update(
                "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                post2.getCreated(), post1.getId()
        );
    }

    @Test
    @DisplayName("Change post author and message")
    void changeAuthorMessage() {
        post2.setCreated(post1.getCreated());

        PostDAO.setPost(post1.getId(), post2);
        Mockito.verify(jdbc).update(
                "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;",
                post2.getAuthor(), post2.getMessage(), post1.getId()
        );
    }

    @Test
    @DisplayName("Change post author and created")
    void changeAuthorCreated() {
        post2.setMessage(post1.getMessage());

        PostDAO.setPost(post1.getId(), post2);
        Mockito.verify(jdbc).update(
                "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                post2.getAuthor(), post2.getCreated(), post1.getId()
        );
    }

    @Test
    @DisplayName("Change post message and created")
    void changeMessageCreated() {
        post2.setAuthor(post1.getAuthor());

        PostDAO.setPost(post1.getId(), post2);
        Mockito.verify(jdbc).update(
                "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                post2.getMessage(), post2.getCreated(), post1.getId()
        );
    }

    @Test
    @DisplayName("Change all fields")
    void changeAll() {
        PostDAO.setPost(post1.getId(), post2);
        Mockito.verify(jdbc).update(
                "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;",
                post2.getAuthor(), post2.getMessage(), post2.getCreated(), post1.getId()
        );
    }
}
